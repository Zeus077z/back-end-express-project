class User {
    constructor(id, albumId, name, description, photo){
        this.id = id;
        this.albumId = albumId;
        this.name = name;
        this.description = description;
        this.photo = photo;
    }
}