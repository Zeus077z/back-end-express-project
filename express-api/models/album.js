class User {
    constructor(id, userId, name, description, creationDate){
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
    }
}