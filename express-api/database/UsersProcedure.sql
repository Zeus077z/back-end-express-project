use proyecto

DELIMITER $$
USE `proyecto`$$

CREATE PROCEDURE `insertupdateuser` (
    IN _id INT,
    IN _name VARCHAR(200),
    IN _lastname VARCHAR(200),
    IN _email VARCHAR(200),
    IN _password VARCHAR(1000),
    IN _dateborn DATE,
    IN _sex VARCHAR(200),
    IN _token VARCHAR(1000)
    )
BEGIN
    IF _id=0 THEN
        INSERT INTO users(id,name,lastname,email,password,dateborn,sex,token)
        VALUES (_id,_name,_lastname,_email,_password,_dateborn,_sex,_token);
        SET _id = LAST_INSERT_ID();
    ELSE
        UPDATE users
        SET id=_id,
        name=_name,
        lastname=_lastname,
        email=_email,
        password=_password,
        dateborn=_dateborn,
        sex=_sex,
        token=_token
        WHERE id=_id;
    END IF;

    SELECT _id AS 'id';
END