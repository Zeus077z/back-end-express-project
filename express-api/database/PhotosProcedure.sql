use proyecto

DELIMITER $$
USE `proyecto`$$

CREATE PROCEDURE `insertupdatephotos` (
    IN _albumId INT,
    IN _id INT,
    IN _title VARCHAR(200),
    IN _url VARCHAR(200),
    IN _thumbnailUrl VARCHAR(200)
    )
BEGIN
    IF _id=0 THEN
        INSERT INTO photos(albumId,id,title,url,thumbnailUrl)
        VALUES (_albumId,_id,_title,_url,_thumbnailUrl);
        SET _id = LAST_INSERT_ID();
    ELSE
        UPDATE photos
        SET albumId=_albumId,
        id=_id,
        title=_title,
        url=_url,
        thumbnailUrl=_thumbnailUrl
        WHERE id=_id;
    END IF;

    SELECT _id AS 'id';
END