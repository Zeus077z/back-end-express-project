use proyecto

DELIMITER $$
USE `proyecto`$$

CREATE PROCEDURE `insertupdatealbums` (
    IN _userId INT,
    IN _id INT,
    IN _title VARCHAR(200),
    IN _description VARCHAR(200)
    )
BEGIN
    IF _id=0 THEN
        INSERT INTO albums(userId,id,title,description)
        VALUES (_userId,_id,_title,_description);
        SET _id = LAST_INSERT_ID();
    ELSE
        UPDATE albums
        SET userId=_userId,
        id=_id,
        title=_title,
        description=_description
        WHERE id=_id;
    END IF;

    SELECT _id AS 'id';
END