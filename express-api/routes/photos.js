var express = require('express');
var router = express.Router();
const mysqlConnection  = require('../database.js');

router.get('/title/:title/:id', (req, res) => {
    const { title } = req.params;
    const { id } = req.params;+ 
    mysqlConnection.query('SELECT photos.* FROM photos INNER JOIN albums on albums.id=photos.albumId where albums.userId=? and photos.title =?',[id,title], (err, rows, fields) => {
      if(!err) {
        res.json(rows);
      } else {
        console.log(err);
      }
    });  
  });

router.get('/:id', (req, res) => {
    const { id } = req.params; 
    mysqlConnection.query('SELECT * FROM photos where albumId=?',[id], (err, rows, fields) => {
      if(!err) {
        res.json(rows);
      } else {
        console.log(err);
      }
    });  
  });

  router.post('/insert', (req, res) => {
    const {albumId,id,title,url,thumbnailUrl}=req.body;
    const query=`CALL insertupdatephotos(?,?,?,?,?);`
    mysqlConnection.query(query,[albumId,id,title,url,thumbnailUrl], (err, rows, fields) => {
      if(!err) {
        res.redirect("http://localhost:3000");
      } else {
        console.log(err);
      }
    });  
  });

  router.post('/delete/:id', (req, res) => {
    const {id}=req.params;
    const query=`delete from photos where id=?`;
    mysqlConnection.query(query,[id], (err, rows, fields) => {
      if(!err) {
        res.redirect("http://localhost:3000");
      } else {
        console.log(err);
      }
    });  
  });
  
  
  

  module.exports = router;