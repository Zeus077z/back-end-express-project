var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
const mysqlConnection = require('../database.js');

router.get('/title/:title/:id', (req, res) => {
  const { title } = req.params;
  const { id } = req.params;

  mysqlConnection.query('SELECT * FROM albums where userId=? and title=?', [id, title], (err, rows, fields) => {
    if (!err) {
      res.json(rows);
    } else {
      console.log(err);
    }
  });
});


router.get('/:id', (req, res) => {
  const { id } = req.params;

  mysqlConnection.query('SELECT * FROM albums where userId=?', [id], (err, rows, fields) => {
    if (!err) {
      res.json(rows);
    } else {
      console.log(err);
    }
  });

  // jwt.verify(req.token, 'secretkey', (error, authData) => {
  //   if (error) {
  //     res.sendStatus(403);
  //   } else {
  //     mysqlConnection.query('SELECT * FROM albums where userId=?', [id], (err, rows, fields) => {
  //       if (!err) {
  //         res.json(rows);
  //       } else {
  //         console.log(err);
  //       }
  //     });

  //   }
  // })

});

function verifyToken(req, res, next) {
  const bearerHeader = req.headers['authorization'];

  if (typeof bearerHeader !== 'undefined') {
    const bearerToken = bearerHeader.split(" ")[1];
    req.token = bearerToken;
    next();
  } else {
    res.sendStatus(403);
  }
};

router.post('/insert', (req, res) => {
  const { userId, id, title, description } = req.body;
  const userId1=parseInt(userId,10);
  const id1=parseInt(id,10);
console.log(req.body);
  const query = `CALL insertupdatealbums(?,?,?,?);`
  mysqlConnection.query(query, [userId1, id1, title, description], (err, rows, fields) => {
    if (!err) {
      res.redirect("http://localhost:3000");
    } else {
      console.log(err);
    }
  });
});

router.post('/delete/:id', (req, res) => {
  const { id } = req.params;
  const query = `delete from albums where id=? `;
  mysqlConnection.query(query, [id], (err, rows, fields) => {
    if (!err) {
      res.redirect("http://localhost:3000");
    } else {
      console.log(err);
    }
  });
});

module.exports = router;