const { query } = require('express');
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
const mysqlConnection = require('../database.js');


router.get('/', (req, res) => {
  mysqlConnection.query('SELECT * FROM users', (err, rows, fields) => {
    if (!err) {
      res.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.get('/:id', (req, res) => {
  const { id } = req.params;
  mysqlConnection.query('SELECT * FROM users where id=?', [id], (err, rows, fields) => {
    if (!err) {
      res.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.post('/login/:username/:password', (req, res) => {
  const { username } = req.params;
  const { password } = req.params;
  mysqlConnection.query('SELECT * FROM users where email=? and password=?', [username, password], (err, rows, fields) => {
    if (!err) {
      res.json(rows);
    } else {
      console.log(err);
    }
  });
});

router.post('/insert', (req, res) => {
  const { id, name, lastname, email, password, dateborn, sex, token } = req.body;
  const query = `CALL insertupdateuser(?,?,?,?,?,?,?,?);`
  mysqlConnection.query(query, [id, name, lastname, email, password, dateborn, sex, token], (err, rows, fields) => {
    if (!err) {
      res.redirect("http://localhost:3000");
    } else {
      console.log(err);
    }
  });
});

router.post('/delete/:id', (req, res) => {
  const { id } = req.params;
  const query = `delete from users where id=?`;
  mysqlConnection.query(query, [id], (err, rows, fields) => {
    if (!err) {
      res.redirect("http://localhost:3000");
    } else {
      console.log(err);
    }
  });
});




module.exports = router;
